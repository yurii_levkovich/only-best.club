<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'portfolio' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8|qr(Io-u9~_&F0bMAXOAn9 |#K_]9|p+`.EmRQoIqx2B$snF(;jGl%?%yIy_876' );
define( 'SECURE_AUTH_KEY',  'J;bzeg}9I]WjzWeWd_13Es3zpKR%5oxG.za57Xshm I%]=?3b]C[{7[F&;r^LxC)' );
define( 'LOGGED_IN_KEY',    'QOf$Y;m(o_o1,rG&g4JpT*!UZTdGArm87&G7V>TQ]#Gd@yV$L}K>+b6D<CeA{z$X' );
define( 'NONCE_KEY',        'nzJf!}#^Mp>Xj^Gssr!0wTu1iYN9J&66_xv,&v_O,{hlPkq#EG`B7jH1R;5oQ,;y' );
define( 'AUTH_SALT',        '/;XRjN%ge6p;,NU<^S?:oAw1WY1,h$;I8=^E!VM|v$FW?d_N19%w1S_75@[qti+u' );
define( 'SECURE_AUTH_SALT', 'h!^X;(plR/9p}%t(FxNg7&VC0b}5aG6o&J*P9b$ig]fEosWPBa4sSKD;W4eUD}`B' );
define( 'LOGGED_IN_SALT',   'PZlDr~bkCV>t(G7dIA[h-1-:<.C<mztRq?;Ld(r=HK#/,[Wv;N3_9C4zb{9^>^~&' );
define( 'NONCE_SALT',       'C7Q^2!JC+eQE8N-f!jXYGjGn]8Mk9Fy~RI!s*edlRT>roQ55;.JIpm~ARv14iT$X' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
